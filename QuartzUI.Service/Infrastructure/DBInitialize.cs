﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using QuartzUI.Code;
using QuartzUI.Code.Model;
using QuartzUI.DataBase;

namespace QuartzUI.Service
{
	/// <summary>
	/// 初始数据库操作类
	/// </summary>
	public class DBInitialize
	{
        private static string cacheKey = GlobalContext.SystemConfig.ProjectPrefix + "_dblist";// 数据库键
        /// <summary>
        /// 获取注册数据库list
        /// </summary>
        /// <param name="readDb">重置数据库list</param>
        /// <returns></returns>
        public static List<ConnectionConfig> GetConnectionConfigs(bool readDb=false)
		{
            List<ConnectionConfig> list = CacheHelper.Get<List<ConnectionConfig>>(cacheKey);
            if (list == null || !list.Any() || readDb)
			{
                list=new List<ConnectionConfig>();
                var data = GlobalContext.SystemConfig;
                var defaultConfig = DBContexHelper.Contex(data.DBConnectionString, data.DBProvider);
                defaultConfig.ConfigId = GlobalContext.SystemConfig.MainDbNumber;
                list.Add(defaultConfig);
                try
                {
                    if (data.SqlConfig == null)
                        data.SqlConfig = new List<DBConfig>();

                    //扩展数据库
                    foreach (var item in data.SqlConfig)
                    {
                        var config = DBContexHelper.Contex(item.DBConnectionString, item.DBProvider);
                        config.ConfigId = item.DBNumber;
						if (list.Any(a=>a.ConfigId == config.ConfigId))
						{
                            throw new Exception($"数据库编号重复，请检查{config.ConfigId}");
						}
                        list.Add(config);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.WriteWithTime(ex);
                }
                CacheHelper.SetBySecond(cacheKey, list);
            }
            return list;
        }
    }
}
