﻿using QuartzUI.Code;
using QuartzUI.DataBase;

namespace QuartzUI.Service
{
	public class DataFilterService<T>: RepositoryBase<T> where T : class, new()
    {
        // 用户信息
        public OperatorModel currentuser;
        // 用于当前表操作
        protected IRepositoryBase<T> repository;
        // 用于其他表操作
        protected IUnitOfWork unitofwork;
        public DataFilterService(IUnitOfWork unitOfWork):base(unitOfWork)
        {
            currentuser = OperatorProvider.Provider.GetCurrent();
            unitofwork = unitOfWork;
            repository = this;
            if (currentuser == null)
            {
                currentuser = new OperatorModel();
            }
        }
    }
}
