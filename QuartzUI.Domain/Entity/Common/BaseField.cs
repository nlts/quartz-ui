﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuartzUI.Domain
{
    public class BaseField
    {
        public static string[] BaseFieldList = new string[]
        {
            "Id",
            "DeleteMark",
            "LastModifyTime",
            "LastModifyUserId",
            "DeleteTime",
            "DeleteUserId",
        };
    }
}
